GitLab Mattermost is a team communication service for sharing messages and
files across PCs and phones, with archiving, instant search and integration 
with GitLab Single-Sign-On. 

*Screenshot: GitLab Mattermost team creation screen with SSO pre-configured from omnibus*

![alt text](http://www.mattermost.org/wp-content/uploads/2015/09/gitlab_mattermost_600.png)

### GitLab Mattermost features include: 

#### Messaging and File Sharing 

- Send messages, comments, files and images across public, private and 1-1 channels
- Personalize notifications for unreads and mentions by channel and keyword
- Use #hashtags to tag and find messages, discussions and files  

#### Archiving and Search

- Import Slack user accounts and channel archives
- Search public and private channels for historical messages and comments
- View recent mentions of your name, username, nickname, and custom search terms

#### Anywhere Access

- Use Mattermost from web-enabled PCs and phones
- Attach sound, video and image files from mobile devices
- Define team-specific branding and color themes across your devices

#### Enterprise Compatibility 

- Self-host Mattermost entirely within your organization's insfrastructure
- LDAP/Active Directory connectivity to Mattermost enabled via GitLab Single-Sign-On

### How to Install GitLab Mattermost

- [GitLab Mattermost Install Guide for omnibus](http://doc.gitlab.com/omnibus/gitlab-mattermost/)
- [GitLab Mattermost SSO Documentation](https://github.com/mattermost/platform/blob/master/doc/integrations/sso/gitlab-sso.md)
- [GitLab Mattermost Troubleshooting Forum](http://forum.mattermost.org/c/general/gitlab)
- [For GitLab 7.14, optionally setup email](https://github.com/mattermost/platform/blob/master/doc/config/smtp-email-setup.md)

### Sharing feedback and filing issues: 

- [Please report security issues via Mattermost Responsible Disclosure Policy](http://www.mattermost.org/responsible-disclosure-policy/)
- [Please file GitLab Mattermost-specific issues to the GitLab Mattermost issue tracker]( 
https://gitlab.com/gitlab-org/gitlab-mattermost/issues).
- [GitLab issues not related to GitLab Mattermost or GitLab omnibus](https://about.gitlab.com/getting-help/).

*Screenshot: Results of importing team from Slack (functionality currently in "Preview" for GitLab 8.0)* 

![alt text](http://www.mattermost.org/wp-content/uploads/2015/09/slack_import_600.png)

*Coming Soon: GitLab 8 color theme and initial support for GitLab Flavored Markdown [(see details)](http://www.mattermost.org/category/blog/) for GitLab 8.1*

![alt text](http://www.mattermost.org/wp-content/uploads/2015/09/gitlab_markdown4.png)

GitLab Mattermost is available under an [MIT license](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/config/software/mattermost.rb) as a compiled version of the [Mattermost open source project](http://www.mattermost.org/).